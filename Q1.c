//Assignment 8
 #include <stdio.h>
  
 struct student{
 	 char name[10];
 	char subject[15];
 	float marks;
 };
int main()
{
   struct student s[5];
   int i;
   for (i=0; i<5; i++)
   {
   	printf("Enter the name of student %d : ",i+1);
   	scanf("%s",&s[i].name);
   	printf("Enter the subject : ",i+1);
   	scanf("%s",&s[i].subject);
   	printf("Enter the marks of the subject : ",i+1 );
   	scanf("%f",&s[i].marks);
   }
   printf("\n");
   for (i=0; i<5; i++)
   {
   	printf("\nStudent %d details\n",i+1 );
   	printf("Name of the student :%s\n", s[i].name);
   	printf("Subject name : %s\n", s[i].subject);
   	printf("Marks for the subject : %f\n", s[i].marks);
   }
  	return 0;
}
